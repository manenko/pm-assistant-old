(ns com.manenko.pm-assistant.resources.views
  (:require [com.manenko.pm-assistant.common.ui.toolbars :as toolbars]
            [com.manenko.pm-assistant.common.ui.tables   :as tables]
            [com.manenko.pm-assistant.resources.db       :as db]
            [com.manenko.pm-assistant.resources.events   :as events]))

(defn resource-toolbar
  [resource]
  (let [resource-id (::db/id resource)
        btn-id      #(str % "-button-" resource-id)]
    [toolbars/toolbar [{::toolbars/button-type ::toolbars/button
                        ::toolbars/id          (btn-id "edit")
                        ::toolbars/title       "Edit"
                        ::toolbars/glyphicon   "edit"
                        ::toolbars/event       [::events/edit-resource resource-id]}
                       {::toolbars/button-type ::toolbars/button
                        ::toolbars/id          (btn-id "raises")
                        ::toolbars/title       "Raises"
                        ::toolbars/glyphicon   "euro"
                        ::toolbars/event       [::events/edit-raises resource-id]}
                       {::toolbars/button-type ::toolbars/button
                        ::toolbars/id          (btn-id "allocations")
                        ::toolbars/title       "Allocations"
                        ::toolbars/glyphicon   "list-alt"
                        ::toolbars/event       [::events/edit-allocations resource-id]}
                       {::toolbars/button-type ::toolbars/dropdown-button
                        ::toolbars/title       "More"
                        ::toolbars/glyphicon   "option-horizontal"
                        ::toolbars/items       [{::toolbars/id    (btn-id "overtimes")
                                                 ::toolbars/title "Overtimes"
                                                 ::toolbars/event [::events/edit-overtimes resource-id]}
                                                {::toolbars/id    (btn-id "lwps")
                                                 ::toolbars/title "Lives without pay"
                                                 ::toolbars/event [::events/edit-lwps resource-id]}
                                                {::toolbars/id    (btn-id "deactivate-resource")
                                                 ::toolbars/title "Deactivate resource"
                                                 ::toolbars/event [::events/deactivate-resource resource-id]}]}]]))

(defn ^:private add-resource-toolbar
  [resources]
  (map #(conj % [::resource-toolbar [resource-toolbar %]])
       resources))

(def resources-table
  (tables/table
   {::tables/headers [{::tables/id          ::db/id
                       ::tables/data-key    ::db/id
                       ::tables/title       "ID"
                       ::tables/sortable?   false
                       ::tables/filterable? false}
                      {::tables/id          ::db/name
                       ::tables/data-key    ::db/name
                       ::tables/title       "Name"
                       ::tables/sortable?   true
                       ::tables/filterable? true}
                      {::tables/id          ::db/project
                       ::tables/data-key    ::db/project
                       ::tables/title       "Project"
                       ::tables/sortable?   true
                       ::tables/filterable? true}
                      {::tables/id          ::db/salary
                       ::tables/data-key    ::db/salary
                       ::tables/title       "Salary"
                       ::tables/sortable?   true
                       ::tables/filterable? false}
                      {::tables/id          ::db/last-raise
                       ::tables/data-key    ::db/last-raise
                       ::tables/title       "Last Raise"
                       ::tables/sortable?   true
                       ::tables/filterable? false}
                      {::tables/id          ::db/planned-raise
                       ::tables/data-key    ::db/planned-raise
                       ::tables/title       "Planned Raise"
                       ::tables/sortable?   true
                       ::tables/filterable? false}
                      {::tables/id          ::db/toolbar
                       ::tables/data-key    ::resource-toolbar
                       ::tables/title       nil
                       ::tables/sortable?   false
                       ::tables/filterable? false}]
    ::tables/group-by  ::db/project
    ::tables/filter-by ::db/name}))
