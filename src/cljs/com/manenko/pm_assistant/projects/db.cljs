(ns com.manenko.pm-assistant.projects.db
  "Defines specs for a project."
  (:require [clojure.spec :as s]
            [com.manenko.pm-assistant.common.id :as id]
            [cljs-time.core :as time]))

;; TODO: goog.date.Date?
(s/def ::date-type  (partial instance? js/Date))

(s/def ::id         string?)
(s/def ::name       string?)
(s/def ::start-date ::date-type)
(s/def ::end-date   ::date-type)

(s/def ::project (s/keys :req [::id ::name ::start-date ::end-date]))

(defn make-project
  [& {:keys [id name start-date end-date]}]
  {::id         (if id id (id/make-id))
   ::name       name
   ::start-date start-date
   ::end-date   (if end-date end-date (time/local-date 2048 12 31))})
