(ns com.manenko.pm-assistant.projects.events
  (:require [re-frame.core :as re-frame]
            [com.manenko.pm-assistant.common.ui.tables :as tables]
            [com.manenko.pm-assistant.projects.subs    :as subs]
            [com.manenko.pm-assistant.projects.db      :as db]))

(re-frame/reg-event-db
 ::initialize
 (fn [db _]
   (assoc db
          ::db/projects-table-schema
          {::tables/subs    {::tables/data-source
                             [::subs/get-projects]}
           ::tables/events  {::tables/on-sort [::sort-by]}
           ::tables/columns [{::tables/id          ::db/name
                              ::tables/selector-fn ::db/name
                              ::tables/title       "Name"
                              ::tables/sorting     ::tables/ascending}
                             {::tables/id          ::db/start-date
                              ::tables/selector-fn ::db/start-date
                              ::tables/title       "Start Date"
                              ::tables/sorting     ::tables/ascending}
                             {::tables/id          ::db/end-date
                              ::tables/selector-fn ::db/end-date
                              ::tables/title       "End Date"
                              ::tables/sorting     ::tables/ascending}
                             {::tables/id          ::project-toolbar
                              ::tables/selector-fn ::project-toolbar}]})))

(re-frame/reg-event-db
 ::sort-by
 (fn [db [_ k]]
   db))
