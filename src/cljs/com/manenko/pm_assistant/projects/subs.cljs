(ns com.manenko.pm-assistant.projects.subs
  (:require [re-frame.core :as re-frame]
            [com.manenko.pm-assistant.projects.db :as db]))

(defn ^:private not-empty?
  [m k]
  (not (empty? (k m))))

(re-frame/reg-sub
 ::initialized?
 (fn [db _]
   (reduce #(and %1 (not-empty? db %2))
           true
           [::db/projects-table-schema])))

(re-frame/reg-sub
 ::get-projects-table-schema
 (fn [db _]
   (::db/projects-table-schema db)))

(re-frame/reg-sub
 ::get-projects
 (fn [db _]
   (::db/projects db)))

