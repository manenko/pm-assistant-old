(ns com.manenko.pm-assistant.projects.views
  (:require [re-frame.core :as re-frame]
            [com.manenko.pm-assistant.common.ui.tables :as tables]
            [com.manenko.pm-assistant.projects.db :as db]
            [com.manenko.pm-assistant.common.ui.toolbars :as toolbars]))

(defn ^:private project-toolbar
  [project]
  (let [project-id (::db/id project)
        btn-id     #(str % "-button-" project-id)]
    [toolbars/toolbar [{::toolbars/button-type ::toolbars/button
                        ::toolbars/id          (btn-id "edit")
                        ::toolbars/title       "Edit"
                        ::toolbars/event       [:com.manenko.pm-assistant.projects.events/edit-project project-id]}]]))

(defn ^:private add-project-toolbar
  [projects]
  (map #(conj % [::project-toolbar [project-toolbar %]])
       projects))

(defn projects-table
  []
  (let [schema (re-frame/subscribe [:com.manenko.pm-assistant.projects.subs/get-projects-table-schema])]
    (fn []
      (tables/table @schema))))
