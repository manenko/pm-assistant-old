(ns com.manenko.pm-assistant.root.views
  (:require [re-frame.core :as re-frame]
            [com.manenko.pm-assistant.common.ui.menus :as menus]
            [com.manenko.pm-assistant.projects.views  :as project-views]))

(defn main-menubar
  []
  [menus/menubar
   {::menus/title "PM Assistant"
    ::menus/menus [{::menus/title "Reports"
                    ::menus/items [{::menus/title "Labor Margin"
                                    ::menus/event [:set-active-panel :reports/labor-margin]}]}]}])

(defn app
  []
  (let [projects (re-frame/subscribe [:com.manenko.pm-assistant.projects.subs/initialized?])]
    (fn []
      (if (and @projects)
        [:div
         [main-menubar]
         [:h1 "Projects"]
         [project-views/projects-table]]
        [:h1 "Initializing..."]))))
