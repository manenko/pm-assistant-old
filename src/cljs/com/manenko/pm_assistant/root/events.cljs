(ns com.manenko.pm-assistant.root.events
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-event-fx
 ::initialize
 (fn [cofx [_]]
   {:dispatch [:com.manenko.pm-assistant.projects.events/initialize]}))
