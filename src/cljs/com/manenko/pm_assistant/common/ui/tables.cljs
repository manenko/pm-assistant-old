(ns com.manenko.pm-assistant.common.ui.tables
  (:require [clojure.spec  :as s]
            [re-frame.core :as re-frame]))

;; Subscriptions
(s/def ::data-source vector?)
(s/def ::subs (s/keys :req [::data-source]))

;; Events
(s/def ::on-sort vector?)
(s/def ::events (s/keys :opt [::on-sort]))

(s/def ::id          keyword?)
(s/def ::selector-fn fn?)
(s/def ::title       string?)
(s/def ::sorting     #{::ascending ::descending})
(s/def ::column      (s/keys :req [::id ::selector-fn]
                             :opt [::title ::sorting]))

(s/def ::columns (s/coll-of ::column))

(s/def ::table (s/keys :req [::columns ::subs]
                       :opt [::events]))

(defn ^:private selector-fns
  [table-schema]
  (map ::selector-fn (::columns table-schema)))

(defn ^:private thead
  [table-schema]
  [:thead (into [:tr] (map #(identity [:th (::title %)])
                           (::columns table-schema)))])

(defn ^:private tr
  [datum selector-fns]
  (into [:tr] (map #(identity [:td (% datum)])
                   selector-fns)))

(defn ^:private tbody
  [data selector-fns]
  (into [:tbody] (map #(tr % selector-fns) data)))

(defn table
  [table-schema]
;  {:pre [(s/valid? ::table table-schema)]}
  (let [selector-fns (selector-fns table-schema)
        data         @(re-frame/subscribe (get-in table-schema [::subs ::data-source]))
        sort-event   (get-in table-schema [::events ::on-sort])
        on-sort      (when sort-event
                        #(re-frame/dispatch (conj sort-event %)))]
    (fn []
      [:table {:class "table table-hover"}
       (thead table-schema)
       (tbody data selector-fns)])))
