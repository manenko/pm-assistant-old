(ns com.manenko.pm-assistant.common.ui.toolbars
  (:require [clojure.spec :as s]
            [re-frame.core :as re-frame]
            [com.manenko.pm-assistant.common.id :as id]))

(s/def ::button-type #{::button ::dropdown-button})
(s/def ::id          string?)
(s/def ::title       string?)
(s/def ::event       vector?)
(s/def ::glyphicon   string?)
(s/def ::item        (s/keys :req [::id ::title ::event]))
(s/def ::items       (s/coll-of ::item))

(defmulti button-type ::button-type)
(defmethod button-type ::button [_]
  (s/keys :req [::button-type ::id ::glyphicon ::event]
          :opt [::title]))
(defmethod button-type ::dropdown-button [_]
  (s/keys :req [::button-type ::glyphicon ::items]
          :opt [::title]))

(s/def ::toolbar-item (s/multi-spec button-type ::button-type))
(s/def ::toolbar (s/coll-of ::toolbar-item))

(defmulti button ::button-type)

(defmethod button ::button
  [button-info]
  [:button {:class    "btn btn-primary btn-xs"
            :title    (::title button-info)
            :on-click #(re-frame/dispatch (::event button-info))}
   [:span {:class (str "glyphicon glyphicon-" (::glyphicon button-info))}]])

(defmethod button ::dropdown-button
  [button-info]
  (let [id (id/make-id)]
    [:div {:class "btn-group"
           :role  "group"}
     [:button {:class "btn btn-default btn-xs dropdown-toggle"
               :type "button"
               :id id
               :title (::title button-info)
               :data-toggle "dropdown"
               :aria-haspopup "true"
               :aria-expanded "true"}
      [:span {:class (str "glyphicon glyphicon-" (::glyphicon button-info))}]]
     (into [:ul {:class "dropdown-menu dropdown-menu-right"
                 :aria-labelledby id}]
           (map #(identity [:li [:a {:href "#"
                                     :on-click (fn [e] (re-frame/dispatch (::event %)))}
                                 (::title %)]])
                (::items button-info)))]))

(defn toolbar
  [data]
  {:pre [(s/valid? ::toolbar data)]}
  (into [:div {:class "btn-toolbar"
               :role "toolbar"}]
        (map button data)))
