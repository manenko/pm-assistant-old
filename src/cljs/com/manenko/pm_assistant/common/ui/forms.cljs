(ns com.manenko.pm-assistant.common.ui.forms
  (:require [re-frame.core :as re-frame]))

(defn form
  "Creates hiccup form element.

  Parameters:
      `type`, if present, should be `:inline` or `:horizontal`.
      `children` is a sequence of children elements of the form."
  [& {:keys [type children]}]
  (let [form-class (when-not (nil? type)
                     (str "form-" (name type)))]
    (into [:form {:class form-class}] children)))

(defn form-group
  [children]
  (into [:div.form-group] children))

(defn input-group
  [children]
  (into [:div.input-group] children))

(defn input-group-addon
  [text]
  (when-not (nil? text)
    [:div.input-group-addon text]))

(defn input
  [& {:keys [id label sr-only? type placeholder value]}]
  [form-group
   [[:label {:for   id
             :class (when sr-only? "sr-only")} label]
    [:input {:type        type
             :placeholder placeholder
             :class       "form-control"
             :id          id
             :value       @value
             :on-change   #(reset! value (-> % .-target .-value))}]]])

(defn button
  [& {:keys [text type on-click]}]
  [:button {:class (str "btn btn-" (if (nil? type)
                                     "primary"
                                     (name type)))
            :type "button"
            :on-click on-click}
   text])

#_(defn input-group-input
  [{:keys [id label type placeholder before after]}]
  [form-group
   [:label {:class "sr-only"
            :for id} label]
   [input-group
    [input-group-addon before]
    [:input {:type type
             :placeholder placeholder
             :class "form-control"
             :id id}]
    [input-group-addon after]]])
