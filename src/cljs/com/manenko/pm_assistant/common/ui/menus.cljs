(ns com.manenko.pm-assistant.common.ui.menus
  (:require [re-frame.core :as re-frame]
            [clojure.spec :as s]))

(s/def ::title string?)
(s/def ::event vector?)

(s/def ::item-type (s/keys :req [::title ::event]))
(s/def ::items (s/coll-of ::item-type))

(s/def ::menu-type (s/keys :req [::title ::items]))
(s/def ::menus (s/coll-of ::menu-type))

(s/def ::menubar-schema (s/keys :req [::title ::menus]))

(defn ^:private navbar
  [children]
  (into [:nav {:class "navbar navbar-inverse navbar-fixed-top"}]
        children))

(defn ^:private container-fluid
  [children]
  (into [:div.container-fluid]
        children))

(defn ^:private navbar-header
  [{:keys [title data-target]}]
  [:div.navbar-header
   [:button {:type          "button"
             :class         "navbar-toggle collapsed"
             :data-toggle   "collapse"
             :data-target   data-target
             :aria-expanded "false"}
    [:span.sr-only "Toggle Navigation"]
    [:span.icon-bar]
    [:span.icon-bar]
    [:span.icon-bar]]
   [:span.navbar-brand title]])

(defn ^:private navbar-collapse
  [{:keys [id children]}]
  (into [:div {:class "collapse navbar-collapse"
               :id    id}]
        children))

(defn ^:private menu-item
  [item]
  [:li [:a {:href     "#"
            :on-click #(re-frame/dispatch (::event item))}
        (::title item)]])

(defn ^:private dropdown-menu
  [menu]
  [:li.dropdown
   [:a {:class         "dropdown-toggle"
        :href          "#"
        :data-toggle   "dropdown"
        :role          "button"
        :aria-haspopup "true"
        :aria-expanded "false"}
    (::title menu) [:span.caret]]
   (into [:ul.dropdown-menu]
         (map menu-item (::items menu)))])

(defn menubar
  [data]
  {:pre [(s/valid? ::menubar-schema data)]}
  [navbar
   [container-fluid
    [navbar-header {:title       (::title data)
                    :data-target "#navbar-collapse"}]
    [navbar-collapse {:id       "navbar-collapse"
                      :children [(into [:ul {:class "nav navbar-nav"}]
                                       (map dropdown-menu (::menus data)))]}]]])
