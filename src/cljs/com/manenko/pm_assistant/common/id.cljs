(ns com.manenko.pm-assistant.common.id
  (:require [cljs-uuid-utils.core :as uuid]))

(defn make-id
  "Make unique string identifier."
  []
  (uuid/uuid-string (uuid/make-random-uuid)))
