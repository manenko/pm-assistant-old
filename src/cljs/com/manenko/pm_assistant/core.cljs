(ns com.manenko.pm-assistant.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [com.manenko.pm-assistant.projects.events]
            [com.manenko.pm-assistant.projects.subs]
            [com.manenko.pm-assistant.root.events :as events]
            [com.manenko.pm-assistant.root.views  :as root]))

(defn init
  []
  (re-frame/dispatch [::events/initialize])
  (reagent/render-component
   [root/app]
   (. js/document (getElementById "app-host"))))
